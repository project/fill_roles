<?php

/**
 * @file fill_roles.admin.inc
 * Functions that are only called on the admin pages.
 */

/**
 * Module settings form.
 */
function fill_roles_settings() {

  $form['fill_roles_info'] = array(
    '#value' => t('<p>The fill_roles module allows you to give roles to a list of users.</p>')
    );

  return system_settings_form($form);
}

/**
 * Function for mass adding roles with expiry date.
 */
function fill_roles_submit($form, &$form_state) {

	// we prepare return arrays
	$added = array();
	$invalid = array();
	$added2 = array();
	$invalid2 = array();
	// we get values
  	$roles_to_add = array_filter($form_state['values']['roles']);
	if (module_exists("role_expire")){
		$expiry_timestamp=strtotime($form_state['values']['expiry_date']);
	}
  	$emails = preg_split("/[\s,]+/", $form_state['values']['emails']);
  	$names = preg_split("/[\s,]+/", $form_state['values']['names']);
  	
  	// we get available roles, to have their names
  	$roles=user_roles();
  	
  	// we parse the emails list
  	foreach ($emails as $email) {
	    $email = trim($email);
	    $email=(string) $email;
	    if ($email!="") {
		    if (valid_email_address($email)) {
	  			if ($account = user_load(array('mail' => $email))){
			    	foreach ($roles_to_add as $rid){
						// Skip adding the role to the user if they already have it.
						if ($account !== FALSE && !isset($account->roles[$rid])) {
							$roles_added = $account->roles + array($rid => $roles[$rid]);
							user_save($account, array('roles' => $roles_added));
						}
				    	if ($expiry_timestamp) {
							//role_expire_set_expiry_date($account->uid, $rid, $expiry_timestamp);
							role_expire_write_record($account->uid, $rid, $expiry_timestamp);
						}
					}
					$added[] =$email;
				}else{
					$invalid[] =$email;
				}
		    }
	    }
	}
	  	
	// we parse the users list
  	foreach ($names as $name) {
	    $name = trim($name);
	    $name=(string) $name;
	    if ($name!=""){
		    if ($account = user_load(array('name' => $name))){
				foreach ($roles_to_add as $rid){
					// Skip adding the role to the user if they already have it.
					if ($account !== FALSE && !isset($account->roles[$rid])) {
						$roles_added = $account->roles + array($rid => $roles[$rid]);
						user_save($account, array('roles' => $roles_added));
					}
					if ($expiry_timestamp) {
						//role_expire_set_expiry_date($account->uid, $rid, $expiry_timestamp);
						role_expire_write_record($account->uid, $rid, $expiry_timestamp);
					}
				}
				$added2[] =$name;
			}else{
				$invalid2[] =$name;
			}
	    }
	}

	if (count($added)>0) {
		$added = implode(", ", $added);
		drupal_set_message(t('The following addresses were updated: %added .', array('%added' => $added)));
		}
	else {
		drupal_set_message(t('No addresses were updated.'));
	}
	if (count($invalid)>0) {
		$invalid = implode(", ", $invalid);
		drupal_set_message(t('The following addresses were invalid: %invalid .', array('%invalid' => $invalid)), 'error');
	}
	if (count($added2)>0) {
		$added2 = implode(", ", $added2);
		drupal_set_message(t('The following users were updated: %added2 .', array('%added2' => $added2)));
		}
	else {
		drupal_set_message(t('No users were updated.'));
	}
	if (count($invalid2)>0) {
		$invalid2 = implode(", ", $invalid2);
		drupal_set_message(t('The following users were invalid: %invalid2 .', array('%invalid2' => $invalid2)), 'error');
	}
	
	
}

/**
 * Function to display mass adding form
 */
function fill_roles() {

  global $language;

  $form['emails'] = array(
    '#type' => 'textarea',
    '#title' => t('Email addresses'),
    '#cols' => 60,
    '#rows' => 5,
    '#description' => t('Email addresses must be separated by comma, space or newline.'),
  );
  $form['names'] = array(
    '#type' => 'textarea',
    '#title' => t('User names'),
    '#cols' => 60,
    '#rows' => 5,
    '#description' => t('User names must be separated by comma, space or newline.'),
  );
	
  // we get available roles
  $roles=user_roles("true");
  // we remove "registrated" role
  //unset($roles[2]);
  //$roles=array_values($roles);
  
  $form['roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Add roles'),
    '#options' => $roles,
    '#required' => TRUE,
  );
  
  if (module_exists("role_expire")){
  	  $form['expiry_date'] = array(
          '#title' => t("Expiry date"),
          '#type' => 'textfield',
          '#attributes' => array('class' => 'role-expire-role-expiry'),
  	  	'#description' => t("Leave blank for indefinite time, enter date and time in format: <em>dd-mm-yyyy hh:mm:ss</em> or use relative time i.e. 1 day, 2 months, 1 year, 3 years.")
      );
  }
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );
  return $form;
}